#include<stdio.h>
#include<stdlib.h>

#include "LinkedList_ArrayOfPointers.h"

int main()
{
	int ans;
	struct LinkedList* linkedlist=NULL;
	struct Node* tempNode=NULL;

	linkedlist=(struct LinkedList*)malloc(sizeof(struct LinkedList));
	if(linkedlist == NULL)
	{
		printf("Main:LinkedList Malloc Failed. Exiting\n");
		exit(1);
	}	
	
	linkedlist->head=NULL;

	while(1)
	{
		if(currentDataCount == MAX)
		{
			currentDataCount=0;
		}

		if(currentDataCount ==0)
		{
			tempNode = createLinkedList(linkedlist,NULL);
		}	
		else
		{
			tempNode = createLinkedList(linkedlist,tempNode);
		}

		printf("Do you want to enter more data(0/1)\t");
		scanf("%d",&ans);

		if(ans == 1)
			continue;
		else
			break;
	}

	printf("\n\nPrinting LinkedList\n");	
	
	printLinkedList(linkedlist); 

	printf("\n\nDeleting the Linked List\n\n");

	uninitialize(linkedlist);

	return 0;
}

struct Node* createLinkedList(struct LinkedList* linkedlist,struct Node* existingNode)
{
	int data,i;
	struct Node* node=NULL;
	struct Node* temp=NULL;

	if(existingNode == NULL)
	{
		node = (struct Node*)malloc(sizeof(struct Node));
		if(node == NULL)
		{
			printf("CreateLinkedList:Node Malloc Failed. Exiting\n");
			exit(0);
		}

		printf("Enter Data:\t");
		scanf("%d",&data);
		
		node->data[currentDataCount] = (int*)malloc(sizeof(int));
		*(node->data[currentDataCount]) = data;
		node->next=NULL;

		if(linkedlist->head == NULL)
		{
			linkedlist->head=node;
		}
		else
		{
			temp = linkedlist->head;

			while(temp->next != NULL)
			{
				temp = temp->next;
			}

			temp->next = node;
		}
	}
	else
	{
		printf("Enter Data:\t");
		scanf("%d",&data);

		existingNode->data[currentDataCount] = (int*)malloc(sizeof(int));
		*(existingNode->data[currentDataCount])=data;
		existingNode->next=NULL;

		node = existingNode;
	}

	currentDataCount++;
	return node;
}

void printLinkedList(struct LinkedList* linkedlist)
{
	struct Node* temp=NULL;
	int i;
	temp = linkedlist->head;

	if(temp == NULL)
	{
		printf("Linked List is empty");
	}
	else
	{
		while(temp != NULL)
		{
			for(i=0;i< MAX && temp->data[i] != NULL;i++)
			{
				printf("%d\t",*(temp->data[i]));
			}

			temp = temp->next;
		}
	}

	printf("\n\n");
}

int uninitialize(struct LinkedList* linkedlist)
{
	struct Node* tempNode=NULL;
	struct Node* tempNode2=NULL;

	int i;

	tempNode = linkedlist->head;

	if(tempNode == NULL)
	{
		return 0;
	}	
	else
	{
		while(tempNode != NULL)
		{
			tempNode2=tempNode->next;
			for(i=0;i<MAX && tempNode->data[i] != NULL;i++)
			{
				printf("Data deleted = %d\n",*(tempNode->data[i]));
			}
			
			free(tempNode);
			tempNode=tempNode2;
		}
	}	

	free(linkedlist);
	linkedlist=NULL;
}