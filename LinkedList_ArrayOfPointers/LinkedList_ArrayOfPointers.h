#define MAX 3

struct Node
{
	int* data[MAX];
	struct Node* next;
};

struct LinkedList
{
	struct Node* head;
};

int currentDataCount=0;

struct Node* createLinkedList(struct LinkedList*,struct Node*);
void printLinkedList(struct LinkedList*);
int uninitialize(struct LinkedList*);