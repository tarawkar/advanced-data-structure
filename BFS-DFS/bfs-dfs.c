/* C program to implement BFS(breadth-first search) and DFS(depth-first search) algorithm */

#include<stdio.h>

int queue[20],top=-1,front=-1,rear=-1,a[20][20],visited[20],stack[20];
int delete();
void add(int item);
void bfs(int s,int n);
void dfs(int s,int n);
void push(int item);
int pop();

void main()
{
	int noOfVertices;
	int i,j,choice;
	int sourceVertex;
	char c;
	
	printf("ENTER THE NUMBER VERTICES ");
	scanf("%d",&noOfVertices);
	for(i=0;i<noOfVertices;i++)
	{
		for(j=0;j<noOfVertices;j++)
		{
			printf("If %d is connected to %d, enter 1 ",i,j);
			scanf("%d",&a[i][j]);
		}
	}
	
	printf("THE ADJACENCY MATRIX IS\n");
	for(i=0;i<noOfVertices;i++)
	{
		for(j=0;j<noOfVertices;j++)
		{
			printf(" %d",a[i][j]);
		}
		printf("\n");
	}

	do
	{
		for(i=0;i<noOfVertices;i++)
		{
			visited[i]=0;
		}
		printf("\n1.BFS");
		printf("\n2.DFS");
		printf("\nENTER YOUR CHOICE");
		scanf("%d",&choice);
		printf("ENTER THE SOURCE VERTEX :");
		scanf("%d",&sourceVertex);

		switch(choice)
		{
			case 1:
					bfs(sourceVertex,noOfVertices);
					break;
			case 2:
					dfs(sourceVertex,noOfVertices);
					break;
		}
		printf("DO U WANT TO CONTINUE(Y/N) ? ");
		scanf(" %c",&c);
	}while((c=='y')||(c=='Y'));
}


	
void bfs(int sourceVertex,int noOfVertices)
{
	int p,i;
	
	add(sourceVertex);
	visited[sourceVertex]=1;
	p=delete();
	
	if(p!=0)
	{
		printf(" %d",p);
	}
	
	while(p!=0)
	{
		for(i=0;i<noOfVertices;i++)
		{
			if((a[p][i]!=0)&&(visited[i]==0))
			{
				add(i);
				visited[i]=1;
			}
		}
		
		p=delete();
		if(p!=0)
			printf(" %d ",p);
	}
	
	for(i=0;i<noOfVertices;i++)
	{
		if(visited[i]==0)
			bfs(i,noOfVertices);
	}	
}


void add(int item)
{
	if(rear==19)
		printf("QUEUE FULL");
	else
	{
		if(rear==-1)
		{
			queue[++rear]=item;
			front++;
		}
		else
			queue[++rear]=item;
	}
}

int delete()
{
	int k;
	if((front>rear)||(front==-1))
		return(0);
	else
	{
		k=queue[front++];
		return(k);
	}
}

void dfs(int sourceVertex,int noOfVertices)
{
	int i,k;
	push(sourceVertex);
	visited[sourceVertex]=1;
	k=pop();
	if(k!=0)
		printf(" %d ",k);
	
	while(k!=0)
	{
		for(i=0;i<noOfVertices;i++)
		if((a[k][i]!=0)&&(visited[i]==0))
		{
			push(i);
			visited[i]=1;
		}
		k=pop();
		if(k!=0)
			printf(" %d ",k);
	}

	for(i=0;i<noOfVertices;i++)
		if(visited[i]==0)
			dfs(i,noOfVertices);
}

void push(int item)
{
	if(top==19)
		printf("Stack overflow ");
	else
		stack[++top]=item;
}

int pop()
{
	int k;
	if(top==-1)
		return(0);
	else
	{
		k=stack[top--];
		return(k);
	}
}

