#include<stdio.h>
#include<stdlib.h>
#include "LinkedList_UsingPointers.h"


int main(int argc, char const *argv[])
{
	/* code */
	int typeOfData;
	struct LinkedList* linkedlist=NULL;
	linkedlist=(struct LinkedList*)malloc(sizeof(struct LinkedList));
	if(linkedlist == NULL)
	{
		printf("Main:LinkedList Malloc Failed. Exiting\n");
		exit(1);
	}	

	linkedlist->head=NULL;
	
	printf("Enter the TYPE of data to be inserted\n1.Integer Numbers\n2.Real Numbers\n3.Characters\n\n=>");
	scanf("%d",&typeOfData);

	createLinkedList(linkedlist,typeOfData);

	printf("\n\nPrinting LinkedList\n");	
	
	printLinkedList(linkedlist,typeOfData);

	printf("\n\nDeleting the Linked List\n\n");

	uninitialize(linkedlist,typeOfData);

	return 0;
}

void createLinkedList(struct LinkedList* linkedlist,int typeOfData)
{
	int ans;
	struct Node* node=NULL;
	struct Node* temp=NULL; 

	while(1)
	{
		node = (struct Node*)malloc(sizeof(struct Node));
		if(node==NULL)
		{
			printf("CreateLinkedList:Node Malloc Failed. Exiting\n");
			exit(0);
		}

		node->data=NULL;
		switch(typeOfData)
		{
			case 1:
				printf("Enter data:");

				node->data = (int*)malloc(sizeof(int));
				scanf("%d",(int*)(node->data));
				node->next = NULL;
			break;
			case 2:
				printf("Enter data:");
				node->data = (float*)malloc(sizeof(float));
				scanf("%f",(float*)(node->data));
				node->next = NULL;
			break;	
			case 3:
				printf("Enter data:");
				node->data = (char*)malloc(sizeof(char));
				scanf(" %c",(char*)(node->data));
				node->next = NULL;
			break;

		}
		
		if(linkedlist->head == NULL)
		{
			linkedlist->head=node;
		}
		else
		{
			temp = linkedlist->head;

			while(temp->next != NULL)
			{
				temp=temp->next;
			}
	
			temp->next = node;
		}	
		

		printf("Do you want to enter more data(1/0):\t");
		scanf("%d",&ans);

		if(ans == 1)
			continue;
		else
			break;
	}
}

void printLinkedList(struct LinkedList* linkedlist,int typeOfData)
{
	struct Node* temp=NULL;

	temp=linkedlist->head;

	if(temp==NULL)
	{
		printf("Linked List is empty");
	}
	else
	{
		while((temp) != NULL)
		{
			switch(typeOfData)
			{
				case 1:
					printf((temp->next == NULL)?"%d":"%d --> ",*((int*)(temp->data)));
					temp=temp->next;	
				break;
				case 2:
					printf((temp->next == NULL)?"%.2f":"%.2f --> ",*((float*)(temp->data)));
					temp=temp->next;	
				break;
				case 3:
					printf((temp->next == NULL)?"%c":"%c --> ",*((char*)(temp->data)));
					temp=temp->next;
				break;
			}
			
		}	
	}
}

int uninitialize(struct LinkedList* linkedlist,int typeOfData)
{
	struct Node* temp=NULL;
	struct Node* temp2=NULL;
	temp=linkedlist->head;

	if(temp == NULL)
	{
		return 0;
	}
	else
	{
		while(temp !=NULL)
		{
			switch(typeOfData)
			{
				case 1:
					temp2=temp->next;
					printf("Data deleted = %d\n",*((int*)(temp->data)));	
					free(temp->data);	
					free(temp);
					temp=temp2;
				break;
				case 2:
					temp2=temp->next;
					printf("Data deleted = %.2f\n",*((float*)(temp->data)));	
					free(temp->data);	
					free(temp);
					temp=temp2;
				break;	
				case 3:
					temp2=temp->next;
					printf("Data deleted = %c\n",*((char*)(temp->data)));	
					free(temp->data);	
					free(temp);
					temp=temp2;
				break;	
			}
		}
	}

	free(linkedlist);
	linkedlist=NULL;
}