struct Node
{
	void* data;
	struct Node* next;
};

struct LinkedList
{
	struct Node* head;	
};
void createLinkedList(struct LinkedList*,int);
void printLinkedList(struct LinkedList*,int);
int uninitialize(struct LinkedList*,int);